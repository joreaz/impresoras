<!DOCTYPE html>
<html>
<head>
  <?php
   include("db.php");
   include("datos_edit_toner.php");
   include("consultas.php");
  ?>
   <link rel="stylesheet" href="css/bootstrap.min.css" >
   <link rel="stylesheet" href="css/bootstrap-theme.min.css">   
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" crossorigin="anonymous">
   <script type="text/javascript" src="js/selecimpresora.js"></script>
  <title>Reporte de impresoras</title>
</head>

<body onload="pintaSelect()">  
  <nav class="navbar navbar-dark bg-dark">
    <div class="container">
      <a href="index.php" class="navbar-dark">Registro de reporte de impresoras</a>
    </div>
  </nav>    

    <div  class="container">  
        <div class="row" >
            <div class="col">
               <h1 >Actualización Toner</h1>
            </div>
        </div>
     </div>

          <div class="container p-8">
              <div class="row">
                <div class="col-md-4 mx-auto">
                  <div class="car card-boddy">
                     <form action="datos_edit_toner.php?id=<?php echo $_GET['id']; ?>" method="POST"  >

                         <div class="card card-body">
                          <h5>Datos del Toner</h5>
                         
                        <div class="form-group">   
                        <input value="<?php echo $id_toner; ?>" type="hidden">
                        <input value="<?php echo $impresora1; ?>" type="hidden">
                        <select id="itoner" name="itoner" class="form-control " class="form-control" placeholder="Clasificación" > <option value="<?php  $tmarca; ?>"><?php echo $tmarca; ?></option>
                                  <option value="GENERICO">GENERICO</option>
                                  <option value="HP">HP</option>    
                                  <option value="SHARP">SHARP</option>    
                                  <option value="EPSON">EPSON</option>    
                                  <option value="LEXMAK">LEXMAK</option> 
                                  <option value="CANON">CANON</option>  
                                  <option value="BROTHER">BROTHER</option>
                                  <option value="XEROX">XEROX</option>Samsung
                                  <option value="DELL">DELL</option>
                                  <option value="SAMSUNG">SAMSUNG</option>
                                  <option value="KODAK">KODAK</option>
                                  <option value="RICOH">RICOH</option>                                                                     
                          </select>
                        </div>

                        <div id="modelo" class="form-group">
                         <input type="text" id="tmodelo" name="tmodelo" value="<?php echo $tmodelo; ?>" class="form-control" placeholder="Modelo actualizado"> 
                        </div>
                       

                         <div class="form-group">
                          <input type="text"id="cantidad" name="cantidad" value="<?php echo $cantidad; ?>" class="form-control" placeholder="Cantidad">     
                            </div>
                         
                          <div class="form-group">
                             <input type="date"  name="caducidad" class="form-control"  value="<?php echo date('Y-m-d', strtotime($caducidad)) ?>">  
                          </div>

                         
                       
                          <h5>Impresoras compatibles con el toner</h5>
                                           

                                     <label>¿Deseas camibar una impresora?&nbsp;<input type="checkbox" id="cboxActiva" value="segundai" onclick="activaSelect()" ></label>

                            <div id="select1" >  
                                      <select id="impresora1" name="impresora1" class="form-control" disabled>

                                        <option value="<?php echo $impresora1; ?>"><?php echo $marcap1['marca'].' Modelo: '.$modelop1['modelo']; ?></option>
                                       <option value="0">Eliminar esta impresora</option>
                                           <?php
                                             while($rowt = mysqli_fetch_array($imdis)){                                          
                                               echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
                                              }
                                               ?>                                                      
                                          </select>
                             </div>
                             <div id="select2"> 
                                  <br>
                              <select id="impresora2" name="impresora2" class="form-control " disabled >    
                                           <option value="<?php echo $impresora2 ?>"><?php echo $marcap2['marca'].' Modelo: '.$modelop2['modelo']; ?></option>
                                            <option value="0">Eliminar esta impresora</option>
                                           <?php
                                                  $imdis = mysqli_query($conn, $queryImDis);
                                             while($rowt = mysqli_fetch_array($imdis)){                                          
                                               echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
                                              }
                                               ?>                                                      
                                          </select>
                             </div> 

                              <div id="select3"> 
                                   <br>
                              <select id="impresora3" name="impresora3" class="form-control" disabled>    
                                           <option value="<?php echo $impresora3 ?>"><?php echo $marcap3['marca'].' Modelo: '.$modelop3['modelo']; ?></option>
                                            <option value="0">Eliminar esta impresora</option>
                                           <?php
                                                  $imdis = mysqli_query($conn, $queryImDis);
                                             while($rowt = mysqli_fetch_array($imdis)){                                          
                                               echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
                                              }
                                               ?>                                                      
                                          </select>
                             </div> 
                     
                              <div id="select4"> 
                                  <br>
                              <select id="impresora4" name="impresora4" class="form-control" disabled>    
                                           <option value="<?php echo $impresora4 ?>"><?php echo $marcap4['marca'].' Modelo: '.$modelop4['modelo']; ?></option>
                                            <option value="0">Eliminar esta impresora</option>
                                           <?php
                                                  $imdis = mysqli_query($conn, $queryImDis);
                                             while($rowt = mysqli_fetch_array($imdis)){                                          
                                               echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
                                              }
                                               ?>                                                      
                                          </select>
                              </div>

                               <label>¿Deseas agregar otra impresora?&nbsp;<input type="checkbox" id="cboxNi" value="segundai" onclick="segundaImp()"></label>

                                 <select id="impresoraN1" style="display:none" name="impresoraN1" class="form-control " >    
                                           <option value="0">Selecciona Impresora</option>
                                           <?php
                                                  $imdis = mysqli_query($conn, $queryImDis);
                                             while($rowt = mysqli_fetch_array($imdis)){
                                                    if (($rowt[id_impresora] <> $impresora1) && ($rowt[id_impresora] <> $impresora2) &&  ($rowt[id_impresora] <> $impresora3) && ($rowt[id_impresora] <> $impresora4)){
                                               echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
                                              }
                                            }
                                           ?>
                                  </select>
                             </div> <button class="btn btn-success" name="update" >Actualiza</button>
                           </div>
                        </div>
                     </div>                                                   
                                
                       
                        
                     
                     </form>
                   
              
              </div>  
          </div>  
    <?php include("include/footer.php"); ?>
