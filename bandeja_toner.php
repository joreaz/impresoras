<?php session_start()?>
<?php

if(!isset($_SESSION['username'])){
header("location:login.php");
}
?>

<?php include("db.php") ?>
<?php include("consultas.php") ?>
<?php include("include/header.php")?>
<?php include("include/footer.php")?>

<body>
     <div  class="container " align="center" >  
        <div class="row" >
            <div class="col">
              <BR>
             <LABEL><h1 >Catalogo de Toner</h1></LABEL>
            </div>
        </div>
     </div>

<div class="container-lg " align="center" >
    <div class="row" >
   	    <div class="col-sm-1.5" align="left">        
           <form action="nuevo_toner.php"  method="POST">
            &nbsp;&nbsp;&nbsp; <LABEL> <button type="submit" class="btn btn-success btn-block" name="nuevo_toner"><i class="fas fa-tint"></i> &nbsp;Agregar</button></LABEL> 
        </div>
         <div class="col-sm-1" align="center">
              <a href="index.php" class="btn btn-outline-success" role="button">Regresar</a>
         </div>
     </div><!--  CIERRA EL ROW -->
   </div>       
             </form>
        
 <div class="container-sm ">
        <?php if(isset($_SESSION['message'])){ ?>
      <div class="alert alert-<?= $_SESSION['message_type']; ?> alert-dismissible fade show" role="alert"><?= $_SESSION['message'] ?>
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
     </div>  
   
             <?php  $_SESSION['message'] = null;} ?>
  </div>
    
 <div class="container-lg" align="center">
   <div class="row" align="center">
     <div class="col-lg-11" align="center">
   	  	<table class="table table-bordered  table-hover" >
   	  	 <thead>
   	  		<tr bgcolor="#cdcdcd">           
   	  			<th>Marca</th>
   	  			<th>Modelo</th>
   	  			<th>Cantidad</th>
   	  			<th>Impresora</th>
   	  			<th>Caducidad</th>
   	  			<th width="15%">Acción</th>
          </tr>
   	  	 </thead>
   	  	  <tbody>
   	  	  	<?php
   	  	  	    while($row = mysqli_fetch_array($toner)){  
                       
                        $queryi = "SELECT * FROM impresoras";
                        $impresoras = mysqli_query($conn, $queryi);                       
                 ?>
                   <tr>                    
                  	 <td><?php echo $row['tmarca'] ?></td>
                  	 <td><?php echo $row['tmodelo'] ?></td>
                  	 <td><?php echo $row['cantidad'] ?></td>
                  	 <td><?php while($rowi = mysqli_fetch_array($impresoras)){                                     
                                 if($rowi['id_impresora'] == $row['impresora1']){
                                      echo $rowi['marca']." &nbsp;Modelo: ".$rowi['modelo'];   ?>
                                       <br>
                                   <?php }                                    
                                  if($rowi['id_impresora'] == $row['impresora2']){
                                       echo $rowi['marca']." &nbsp;Modelo: ".$rowi['modelo'];   ?>
                                       <br>                                     
                                    <?php }
                                  if($rowi['id_impresora'] == $row['impresora3']){
                                       echo $rowi['marca'];  ?>
                                          &nbsp;Modelo:
                                           <?php    
                                       echo $rowi['modelo']; ?>
                                       <br>
                                    <?php  }
                                     if($rowi['id_impresora'] == $row['impresora4']){
                                        echo $rowi['marca'];  ?>
                                          &nbsp;Modelo:
                                           <?php    
                                       echo $rowi['modelo']; ?>
                                       <br>    
                                   <?php   }
                                }?>
                              </td>
                  	 <td><?php echo date("d-m-Y",strtotime($row['caducidad'])); ?></td>
                     <td>
                     	<a href="edit_toner.php?id=<?php echo $row['id_toner']?>" class="btn btn-secondary"><i class="fas fa-marker"></i></a>&nbsp<a href="eliminar_toner.php?id=<?php echo $row['id_toner']?>" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
                       </td>                        
                  </tr>
   	  	  	     <?php } ?> 	       

   	  	  </tbody>
        </table>
   	  </div>
    </div>
   </div>  	
</div>	
</body>
</html>