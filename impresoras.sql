-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-03-2020 a las 01:24:39
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `impresoras`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impresoras`
--

CREATE TABLE `impresoras` (
  `id_impresora` int(11) NOT NULL,
  `marca` varchar(250) NOT NULL,
  `modelo` varchar(250) NOT NULL,
  `toner` int(11) NOT NULL,
  `categoria` varchar(250) NOT NULL,
  `ubicacion` int(11) NOT NULL,
  `reporte` varchar(250) DEFAULT NULL,
  `nSerie` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `impresoras`
--

INSERT INTO `impresoras` (`id_impresora`, `marca`, `modelo`, `toner`, `categoria`, `ubicacion`, `reporte`, `nSerie`) VALUES
(5, 'SHARP', 'MX-M264N', 0, 'MEDIA', 0, 'FUNCIONANDO', '2506513Y'),
(6, 'SHARP', 'MX-M266N', 0, 'MEDIA', 4, 'FUNCIONANDO', '75006874'),
(7, 'SHARP', 'MX-M266N', 0, 'MEDIA', 6, 'FUNCIONANDO', '2506444Y'),
(8, 'SHARP', 'MX-M3115N', 0, 'MEDIA', 5, 'FUNCIONANDO', '35018667'),
(9, 'SHARP', 'MX-M3115N', 0, 'ALTA', 13, 'FUNCIONANDO', '45058921');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportes`
--

CREATE TABLE `reportes` (
  `id_reportes` int(11) NOT NULL,
  `impresora` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `n_impresiones` int(11) NOT NULL,
  `status` varchar(250) NOT NULL,
  `fecha` datetime NOT NULL,
  `nImpresionesante` int(11) NOT NULL,
  `porcentaje` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `toner`
--

CREATE TABLE `toner` (
  `id_toner` int(11) NOT NULL,
  `tmarca` varchar(250) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `impresora1` int(11) DEFAULT NULL,
  `impresora2` int(11) DEFAULT NULL,
  `impresora3` int(11) DEFAULT NULL,
  `impresora4` int(11) DEFAULT NULL,
  `tmodelo` varchar(11) NOT NULL,
  `caducidad` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacion`
--

CREATE TABLE `ubicacion` (
  `id_ubicacion` int(11) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `ubicacion` varchar(250) NOT NULL,
  `area` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ubicacion`
--

INSERT INTO `ubicacion` (`id_ubicacion`, `direccion`, `ubicacion`, `area`) VALUES
(4, 'Av. Miguel Aleman 109', 'Gerencia de Administración', 'Frente a Oficina del Jefe de Adquisiciones'),
(5, 'Av. Miguel Aleman 109', 'Unidad de Informática', 'Oficina de Informática Cerca de Catastro'),
(6, 'Av. Miguel Aleman 109', 'Dirección General', 'Afuera de la Oficina de la Dirección de Finanzas.'),
(7, 'Av. Miguel Aleman 109', 'Gerencia de Planeación', 'Frente a Contraloría'),
(8, 'Av. Miguel Aleman 109', 'Controlaría Interna', 'Junto a Sala de Capacitación'),
(9, 'Av. Miguel Aleman 109', 'Coordinación de Asesores', 'Junto a Dirección General'),
(10, 'Av. Miguel Aleman 109', 'Coordinación Gdes. Usuarios', 'Junto a Gerencia Comercial'),
(11, 'Av. Miguel Aleman 109', 'Coordinación Acceso a Inf. Pub.', 'Junto a Pago Anual'),
(12, 'Av. Miguel Aleman 109', 'Coordinación Jurídica', 'Junto a Prodi'),
(13, 'Av. Miguel Aleman 109', 'Coordinación de Agua y Vinculación S.', 'Junto a Informática '),
(14, 'Av. Miguel Aleman 109', 'Gerencia de Recursos Humanos', '3er. Piso'),
(15, 'Av. Miguel Aleman 109', 'Dirección de Operación y Mantto.', 'Final de Estacionamiento'),
(16, 'Av. Miguel Aleman 109', 'Unidad de Ejecución Fiscal', 'Junto a Gerencia Comercial - Cajas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuarios` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `cargo` varchar(250) NOT NULL,
  `login` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuarios`, `nombre`, `cargo`, `login`, `pass`) VALUES
(1, 'Cuitlahuac', 'Analista Informatico ', 'admin', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `impresoras`
--
ALTER TABLE `impresoras`
  ADD PRIMARY KEY (`id_impresora`);

--
-- Indices de la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD PRIMARY KEY (`id_reportes`);

--
-- Indices de la tabla `toner`
--
ALTER TABLE `toner`
  ADD PRIMARY KEY (`id_toner`);

--
-- Indices de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`id_ubicacion`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `impresoras`
--
ALTER TABLE `impresoras`
  MODIFY `id_impresora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `reportes`
--
ALTER TABLE `reportes`
  MODIFY `id_reportes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `toner`
--
ALTER TABLE `toner`
  MODIFY `id_toner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `id_ubicacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
