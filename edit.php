<?php session_start()?>
<?php

if(!isset($_SESSION['username'])){
header("location:login.php");
}
?>
<?php
   include("db.php");
   include("datos_edit.php");
   include("consultas.php");
  ?>
    <?php include("include/header.php"); ?>
    <div  class="container">  
        <div class="row" >
            <div class="col">
               <h1 align="center" >Actualización de impresora</h1>
            </div>
        </div>
     </div>

          <div class="container p-4">
              <div class="row">
                <div class="col-md-4 mx-auto">
                  <div class="car card-boddy">
                     <form action="datos_edit.php?id=<?php echo $_GET['id']; ?>" method="POST" >
                         <div class="card card-body">
                          <h5>Datos de la impresora</h5>
                        <div class="form-group">   
                        <input value="<?php echo $id_impresora; ?>" type="hidden"  >                     
                        <select id="iImpresora" name="iImpresora" class="form-control " class="form-control" placeholder="Clasificación" >          <option value="<?php  $marca; ?>"><?php echo $marca; ?></option>
                                  <option value="HP">HP</option>    
                                  <option value="SHARP">SHARP</option>    
                                  <option value="EPSON">EPSON</option>    
                                  <option value="LEXMAK">LEXMAK</option> 
                                  <option value="CANON">CANON</option>  
                                  <option value="BROTHER">BROTHER</option>
                                  <option value="XEROX">XEROX</option>Samsung
                                  <option value="DELL">DELL</option>
                                  <option value="SAMSUNG">SAMSUNG</option>
                                  <option value="KODAK">KODAK</option>
                                  <option value="RICOH">RICOH</option>                                                                     
                          </select>
                        </div>

                        <div id="modelo" class="form-group">
                         <input type="text" id="modelo" name="modelo" value="<?php echo $modelo; ?>" class="form-control" placeholder="Modelo actualizado"> 
                        </div>
                       

                         <div class="form-group">
                          <input type="text" name="nSerie" value="<?php echo $nSerie; ?>" class="form-control" placeholder="Numero de serie actualizado">     
                            </div>
                         

                          <div class="form-group">                         
                        <select id="categoria" name="categoria" class="form-control ">       <option value="<?php echo $categoria; ?>"><?php echo $categoria; ?></option>
                               <option value="BAJA">BAJA</option>
                                <option value="MEDIA">MEDIA</option>
                                 <option value="ALTA">ALTA</option>

                        </select>
                        </div>
                         </div>
                       
                     <div class="card card-body">
                        <div  class="container">  
                            <div class="row" >
                               <div class="col">
                                  <div class="car card-boddy">
                                  <h5>Ubicación</h5>
                               </div>
                            </div>
                        </div>
                        

                         <div class="car card-boddy">
                           <div class="form-group">
                                  <select id="iubicacion" name="iubicacion" class="form-control " class="form-control" placeholder="Clasificación" >
                                      <option value="<?php $id_ubicacion; ?>"><?php echo $direccion; ?>  <?php echo $ubicacion; ?></option>
                                  <?php while($rowu = mysqli_fetch_array($ubicacionq)){
                                       echo '<option value="'.$rowu[id_ubicacion].'">'.$rowu[direccion].' &nbsp;&nbsp; Ubicación:'.$rowu[ubicacion].'</option>';
                                      }
                                  ?>                                                      
                          </select>   
                                 </div>
                        </div>
                        </div> 
                          </div><br>
                          <div align="center">
                       <button class="btn btn-success" name="update">Actualiza</button><a href="index.php" class="btn btn-outline-success" role="button">Regresar</a>
                          </div>
                     </form>
                   
              
              </div>  
          </div>  
    <?php include("include/footer.php"); ?>
