<?php
   include("db.php");
   include("datos_edit.php");
   include("consultas.php");
  ?>
<?php include("include/header.php"); ?>

<div class="car">
    <h1 align="center" >Reporte de Impresora</h1>
</div>  

<div class="container p-4">
  <div class="car">
     <div class="card-body"> 
        <form action="guardar_reporte.php?id=<?php echo $_GET['id']; ?>" method="POST" >
        <h3 align="center">Reportes de la impresora  <?php echo $marca;?>:&nbsp;<?php echo $modelo;?> &nbsp;&nbsp;</h3>
        <h4 align="center">Numero de serie:&nbsp; <?php echo $nSerie;?> </h4>
    </div>
  </div>
</div>

<div class="container-fluid-xl" align="center"> 
  <table class="table table-hover" cellpadding="1" cellspacing="1">
    <thead class="thead-light">
   <!--   <tr>        
        <th id="encabzp" >&nbsp;MARCA</th>
        <th id="encabzp" >&nbsp;MODELO</th>
        <th id="encabzp" >&nbsp;NUMERO DE SERIE</th>
        <th id="encabzp" >&nbsp;DIRECCIÓN</th>
        <th id="encabzp" >&nbsp;UBICACIÓN</th>
        <th id="encabzp" ></th>
     </tr> 
     </tr> 
     <tr>        
        <th id="encabzp" >&nbsp;<?php echo $marca;?></th>
        <th id="encabzp" >&nbsp;<?php echo $modelo;?></th>
        <th id="encabzp" >&nbsp;<?php echo $nSerie;?></th>
        <th id="encabzp" >&nbsp;<?php echo $direccion;?></th>
        <th id="encabzp" >&nbsp;<?php echo $ubicacion;?></th>
        <th id="encabzp" ></th>
     </tr> -->
      <tr>        
       <!-- <th id="encabzp" >&nbsp;No. Rep</th>  -->
        <th id="encabzp" >&nbsp;REPORTE</th>
        <th id="encabzp" >&nbsp;No. Impresiones</th>
        <th id="encabzp" >&nbsp;PORCENTAJE</th>
        <th id="encabzp" >&nbsp;FECHA</th>
        <th id="encabzp" >&nbsp;DESCRIPCION</th>
     </tr> 
     </thead>
     <tbody>
        <?php
           while($row = mysqli_fetch_array($reportes)){?>
            <tr>
           <!--    <td><?php // echo $row['id_reportes'] ?></td>-->
             <?php 
                   if($_GET['id'] == $row['impresora']){
                    $por = $row['porcentaje'];
              ?>
              <td width="25%"><?php echo $row['status'] ?></td>
              <td width="10%"><?php echo $row['n_impresiones'] ?></td>
              <td width="15%" align="center"><?php echo $row['porcentaje'] ?>% 
                  
                   <progress class="progress-bar" value="<?php echo $row['porcentaje'] ?>" max=100></progress>
                  </td>
              <td width="15" ><?php echo date('Y-m-d', strtotime($row['fecha'])) ?></td>
              <td width="35%"><?php echo $row['descripcion'] ?></td>            
            </tr>
              <?php
                   }
             ?>
         <?php } ?>
     </tbody>
   </table>
</form>
<?php include("include/footer.php"); ?>
</body>
</html>
                        
