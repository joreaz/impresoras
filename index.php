<?php session_start()?>
<?php

if(!isset($_SESSION['username'])){
header("location:login.php");
}
?>

<?php include("db.php") ?>
<?php include("consultas.php") ?>
<?php include("include/header.php")?>
<?php include("include/footer.php")?>

<script type="text/javascript" src="js/selecimpresora.js"></script>

<body>
     <div  class="container">  
        <div class="row" >
            <div class="col">
              <br>
               <h1 align="center" >Catalaogo de impresoras</h1>
               <br>
            </div>
        </div>
     </div>

<div class="container">
    <div class="row">
      
   	  <div class="col ">
         <div class="btn-group">        
            <form action="nueva_impresora.php" id="impresorasf" name="impresorasf" method="POST">
           <label>  <button type="submit" class="btn btn-success btn-block" name="nueva_impresora"><i class="fas fa-print"></i> Agregar</button></label>               
            </form>
           <form action="bandeja_toner.php" >
          <label>  <button type="submit" class="btn btn-outline-success " name="nueva_impresora"><i class="fas fa-tint"></i> &nbsp;Toners </button> </label>              
          </form>
           <form action="ubicacion.php" >
          <label>  <button type="submit" class="btn btn-outline-success " name="nueva_ubicacion"><i class="fas fa-map-marked-alt"></i> &nbsp;Ubicacion </button> </label>            
          </form>
           <form action="cerrar.php" >
          <label>  <button type="submit" class="btn btn-outline-success " name="nueva_ubicacion"><i class="fas fa-sign-out-alt"></i> &nbsp;Salir </button> </label>       
          </form>
      </div>	
     </div>
    </div>
    </div>


  <div class="container">  
    <div class="row">
         <div class="col">
           <?php if(isset($_SESSION['message'])){ ?>
              <div class="alert alert-<?= $_SESSION['message_type']; ?> alert-dismissible fade show" role="alert">
                 <?= $_SESSION['message'] ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>                       
                 </button>
             </div>  
        </div>      
                    <?php   } 
                     $_SESSION['message'] = null;
                    ?>
    
    <div class="row">
      <div class="col">
   	  	<table class="table table-bordered table-hover">
   	  	 <thead>
   	  		<tr bgcolor="#cdcdcd">
   	  			<th>Marca</th>
   	  			<th>Modelo</th>
   	  			<th>No. Serie</th>
   	  			<th>Categoria</th>
   	  			<th width="25%">Dirección</th>
            <th width="12%">Área</th>
            <th>Estatus</th>
                <th >Acción</th>
          </tr>
   	  	 </thead>
   	  	  <tbody>
   	  	  	<?php
   	  	  	    while($row = mysqli_fetch_array($tindex)){?>
                  <tr>
                  	 <td><?php echo $row['marca'] ?></td>
                  	 <td><?php echo $row['modelo'] ?></td>
                  	 <td><?php echo $row['nSerie'] ?></td>
                  	 <td><?php echo $row['categoria'] ?></td>
                  	 <td><?php echo $row['direccion'] ?></td>
                     <td><?php echo $row['ubicacion'] ?></td>
                     <td><?php echo $row['reporte']?></td>
                     <td width="22%">
                     	<a href="edit.php?id=<?php echo $row['id_impresora']?>" class="btn btn-secondary"><i class="fas fa-marker" alt="editar" title="Editar datos"></i></a>&nbsp<a href="eliminar.php?id=<?php  echo $row['id_impresora']?>" onclick="preguntar()" class="btn btn-danger"  ><i class="far fa-trash-alt" alt="descripcion" title="Elimina Impresora"></i></a>&nbsp<a href="reporte.php?id=<?php echo $row['id_impresora']?>" class="btn btn-info"><i class="fas fa-clipboard-list" alt="reporte" title="Genera Reporte"></i></a>
                       </td>
                            
                        
                  </tr>
   	  	  	     <?php } ?> 	       

   	  	  </tbody>
        </table>
   	  </div>
   </div>  	
	</div>
   <?php include("include/footer.php"); ?>
</body>
</html>