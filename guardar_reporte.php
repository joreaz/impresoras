
<meta  http-equiv="Content-type" content="text/html; charset=utf-8">
<?php 
   include("include/header.php");
   include("db.php");

   header("Content-Type: text/html;charset=utf-8");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'mailserver/Exception.php';
require 'mailserver/PHPMailer.php';
require 'mailserver/SMTP.php';

$miFecha = date("h : i : a Y-m-d");

function fechaEspañol ($fecha) {
 //$fecha = substr($fecha, 0, 25);
 // $numeroDia = date('d', strtotime($fecha));
  $numeroDia = date('d');
  $dia = date('l');
  $mes = date('F');
  $anio = date('Y');
  $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
  $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
  $nombredia = str_replace($dias_EN, $dias_ES, $dia);
$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
  //return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;
    return $nombredia." ".$numeroDia." de ".$nombreMes." de ".date("Y");
 // return $mes;

}


//echo $miFecha;
//echo fechaEspañol($miFecha);
//Europe/Berlin





 if (isset($_POST['reporte'])){
	$impresora = $_POST['id_impresora'];
	$marca = $_POST['marca'];
	$modelo = $_POST['modelo'];
	$nserie = $_POST['nserie'];
	$status = $_POST['tReporte'];
//	echo $status;
	$descripcion = $_POST['descripcion'];
	$n_impresiones = $_POST['paginas'];
	$porcentaje = $_POST['porcentaje'];
	$fecha = $_POST['fecha'];
	$ubicacion = $_POST['iubicacion'];

 // echo $ubicacion;

   $query2="SELECT * FROM ubicacion WHERE id_ubicacion = '$ubicacion'";
   $result2=mysqli_query($conn, $query2);
   $rowt = mysqli_fetch_array($result2);
  
   $direccion = $rowt['direccion'];
   $area = $rowt['area'];
   $ubicacionR = $rowt['ubicacion'];

   //echo $ubicacionR;
	
	$query = "INSERT INTO reportes(id_reportes, impresora, usuario, descripcion, n_impresiones, porcentaje, status, fecha, nImpresionesante) VALUES (NULL,'$impresora', '0', '$descripcion','$n_impresiones', '$porcentaje', '$status', '$fecha', '0' )";
	$result= mysqli_query($conn, $query);
	if (!$result){
		 die("Query failed");
	}

    
      if($ubicacion <> NULL){

         $queryUbicacion = "UPDATE impresoras set  ubicacion = $ubicacion  WHERE id_impresora = $impresora ";
          mysqli_query($conn, $queryUbicacion);
        }
    if (($status <> 'CAMBIO DE TONER') && ($status <> 'VERIFICACIÓN DE TONER') && ($status <> 'CAMBIO DE UBICACIÓN') ) {
       
      $qu = "UPDATE impresoras set  reporte = '$status'  WHERE id_impresora = $impresora ";
          mysqli_query($conn, $qu);
     }  


     if ($status == 'REPORTADA'){
        
        $mail = new PHPMailer(true);

try {
    //Server settings
   // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
	$mail->SMTPDebug = 0;
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'uidscmas@gmail.com';                     // SMTP username
    $mail->Password   = 'impresoras';                               // SMTP password
    $mail->SMTPSecure = 'tls';
   // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('uidscmas@gmail.com', 'Reporte de impresoras CMAS');
    $mail->addAddress('joreazmail@gmail.com', 'Reporte de impresoras CMAS');     // Add a recipient
    $mail->addAddress('joreaz@hotmail.com');               // Name is optional
   // $mail->addReplyTo('info@example.com', 'Information');
  //  $mail->addCC('cc@example.com');
 //   $mail->addBCC('bcc@example.com');

    // Attachments
  //  $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
  //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    //$mail->isHTML(true);                                  // Set email format to HTML
    //$mail->Subject = 'Reporte de impresora'.$marca.' '.$modelo.'';

    $body = "Impresora:  " .$marca;
    $body .= "<br>";
    $body .= "<br>";
    $body .= "Modelo: ".$modelo;
    $body .= "<br>";
    $body .= "<br>";
    $body .= "No. Serie: ".$nserie;
    $body .= "<br>";
    $body .= "<br>";
    $body .= "<br>";
    $body .= "Se requiere de su apoyo para atender la solicitud de mantenimiento de la impresora  <b>" .$marca. "</b> modelo <b>"  .$modelo.  "</b> con numero de serie <b>"  .$nserie.   " </b> instalada en  <b>"  .$ubicacionR. "</b>  (" .$direccion. ")  la cual presenta el siguiente problema:  <b>" .$descripcion.  " </b> ";
  //  $body .= $marca. "instalada en ";
  //  $body .= $ubicacionR;
  //  $body .= "la cual presenta el siguiente problema " .$descripcion; 
   // $body .= $descripcion;
   // $body .= "Dicho reporte se generó a las " .date("h:i: Y-m-d");
      $body .= "Dicho reporte se generó a las <b>" .date("h : i a")." el "  .fechaEspañol($miFecha). " </b> ";
    $body .= "<br>";
    $body .= "<br>";
    $body .= "De acuerdo con lo establecido en las bases de licitación anexo 8, donde se comprometen bajo protesta de decir verdad, a cumplir en un plazo no mayor a 2 horas, sírvase la presente como evidencia y medio de reporte.";
    
    $mail->Subject = 'Reporte de impresora';
    $mail->Body    = $body;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
  //  echo 'EL mensaje se ha enviado satisfactoriamente';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}


     }
        
//echo $body;

	//header("location: index.php");
  
}
  $_SESSION['message'] = 'Reporte agregado satisfactoriamente';
  $_SESSION['message_type'] = 'success';

//header("location: index.php");
  


	
  ?>
