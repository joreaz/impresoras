<?php session_start()?>
<?php

if(!isset($_SESSION['username'])){
header("location:login.php");
}
?>

<?php   
   include("db.php");
   include("consultas.php");
?>
    <?php include("include/header.php"); ?>

      <div  class="container">  
        <div class="row" align="center" >
            <div class="col ">
               <div class="car card-boddy">
               <h1 align="center" >Nueva impresoras</h1>
             </div>
            </div>
        </div>
     </div>

       <div class="container p-4">
         <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">
               <h3 >Datos Generales</h3>
             </div>      
           </div>
         </div>
         <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">
                <form id="nuevaimpresora" name="nuevaimpresora" action="guardar_impresora.php" method="POST" >
                 <div class="form-group" >
                 <select id="imarca" name="imarca" class="form-control " required="" >                 <option value="Selecciona">SELECCIONA IMPRESORA</option> 
                                  <option value="HP">HP</option>    
                                  <option value="SHARP">SHARP</option>    
                                  <option value="EPSON">EPSON</option>    
                                  <option value="LEXMAK">LEXMAK</option> 
                                  <option value="CANON">CANON</option>  
                                  <option value="BROTHER">BROTHER</option>
                                  <option value="XEROX">XEROX</option>
                                  <option value="DELL">DELL</option>
                                  <option value="SAMSUNG">SAMSUNG</option>
                                  <option value="KODAK">KODAK</option>
                                  <option value="RICOH">RICOH</option> 
                                  <option value="KIOSERA">KIOSERA</option>                                 
                          </select>                              
                        </div>
                </div>
            </div>
          </div>
        <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">
                        <div class="form-group">
                          <input type="text" name="modelo" value="" required="" class="form-control" placeholder="Modelo de impresora">     
                        </div>
                </div>
          </div>
        </div>     
              <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">
                         <div class="form-group">
                          <input type="text" name="nSerie" value="" required="" class="form-control" placeholder="Numero de Serie">     
                         </div>
              </div>
          </div>
        </div>   
          <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">
                        <div class="form-group">
                           <select id="iCat" name="iCat" class="form-control" placeholder="Clasificación" >    
                                  <option value="Clasificación">SELECCIONA LA CLASIFICACIÓN</option>
                                  <option value="ALTA">ALTA</option>    
                                  <option value="MEDIA">MEDIA</option>    
                                  <option value="BAJA">BAJA</option>                                      
                          </select> 
                     </div>
                 </div>
          </div>
        </div> 
         <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">                                                
                        <div class="form-group">
                     
                        </div>
               </div>
          </div>
        </div>                                          
            <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">       <h3 >Ubicación</h3>
                                  <div class="form-group">
                                  <select id="iubicacion" name="iubicacion" class="form-control" placeholder="Clasificación" >    <option value="SELECCIONA">Selecciona Ubicación</option>
                                  <?php while($rowu = mysqli_fetch_array($ubicacionq)){
                                       echo '<option value="'.$rowu[id_ubicacion].'">'.$rowu[direccion].' &nbsp;&nbsp; Ubicación:'.$rowu[ubicacion].'</option>';
                                      }
                                  ?>                                                      
                                   </select>   
                                 </div>

                             <div align="center">
                            <button name="nuevaimpresora" class="btn btn-success" type="submit">Guardar</button><a href="index.php" class="btn btn-outline-success" role="button">Regresar</a></div>
                           </div>
                   </div>
                  </div>   
                     </form>
                  </div>  
                
              </div>  
          </div>  
    <?php include("include/footer.php"); ?>
