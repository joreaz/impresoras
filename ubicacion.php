<?php session_start()?>
<?php

if(!isset($_SESSION['username'])){
header("location:login.php");
}
?>

<?php   
   include("db.php");
   include("consultas.php");
?>
    <?php include("include/header.php"); ?>

    <div  class="container">  
        <div class="row" align="center" >
            <div class="col ">
               <div class="car card-boddy">
           <label> <h1 align="center" >Nueva Ubicacion</h1></label>
             </div>
            </div>
        </div>
     </div>

       <div class="container p-4">
         <div class="row">
           <div class="col-md-4 mx-auto" align="center">
             <div class="car card-boddy">
               <label><h3 >Datos Generales</h3></label>
             </div>      
           </div>
         </div>
         <div class="row">
           <div class="col-md-4 mx-auto">             
                <form id="nuevaUb" name="nuevaUb" action="guardar_ubicacion.php" method="POST" >
                  <div class="car card-boddy">
                       <input type="text" name="direccion" id="direccion" value="" required="" class="form-control" placeholder="Dirección">     
                  </div>            
          </div>
        </div>     

        <div class="row">
          <div class="col-md-4 mx-auto">
             <label>
             <input type="text" name="ubicacion" id="ubicacion" value="" required="" class="form-control" size="60" placeholder="Área o Departamento">     
             </label> 
         </div>
        </div>   
          <div class="row">
           <div class="col-md-4 mx-auto">
             <div class="car card-boddy">
                <label>
                     <textarea id="descripcion" name="descripcion" rows="3" value="" required="" class="form-control" cols="46" placeholder="Descripción de la Ubicación"></textarea>
                </label>           
            </div>
          </div>
        </div> 
                            <br>
                            <br>          
                          <div align="center">
                            <button name="nuevaUbicacion" class="btn btn-success" type="submit">Guardar</button><a href="index.php" class="btn btn-outline-success" role="button">Regresar</a></div>
                       </div>
                 </form>
           </div>  
          </div>  
    <?php include("include/footer.php"); ?>
