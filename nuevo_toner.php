<?php session_start()?>
<?php

if(!isset($_SESSION['username'])){
header("location:login.php");
}
?>

<?php   
   include("db.php");
   include("consultas.php");          
?>
<?php include("include/header.php"); ?>
<script type="text/javascript" src="js/selecimpresora.js"></script>
<script>

</script>

<div  class="container" align="center">  
  <div class="row" >
   <div class="col">
       <label><h1 >Nuevo Toner</h1></label>
   </div>
  </div>
</div>

<div  class="container">  
  <div class="row" >
     <div class="col" align="center">
     <label><h3 >Datos Generales</h3></label>
      </div>
    </div>
  </div>

<div class="container-sm p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="car card-boddy">
        <form id="nuevoToner" name="nuevoToner" action="guarda_toner.php" method="POST" >
          <div class="form-group">
           <select id="tmarca" name="tmarca" class="form-control  " placeholder="Selecciona la Marca" >    
                                  <option value="0">SELECCIONA TONER</option> 
                                  <option value="GENERICO">GENERICO</option>
                                  <option value="HP">HP</option>    
                                  <option value="SHARP">SHARP</option>    
                                  <option value="EPSON">EPSON</option>    
                                  <option value="LEXMAK">LEXMAK</option> 
                                  <option value="CANON">CANON</option>  
                                  <option value="BROTHER">BROTHER</option>
                                  <option value="XEROX">XEROX</option>Samsung
                                  <option value="DELL">DELL</option>
                                  <option value="SAMSUNG">SAMSUNG</option>
                                  <option value="KODAK">KODAK</option>
                                  <option value="RICOH">RICOH</option>
                                    <option value="KIOSERA">KIOSERA</option>                                   
                          </select>                              
                        </div>

                        <div class="form-group">
                          <input type="text" name="tmodelo" value="" required="" class="form-control" placeholder="Modelo del Toner">     
                        </div>
                         
                         <div class="form-group">
                          <input type="number" name="cantidad" value="" required="" class="form-control" placeholder="Cantidad">
                         </div>
                        <div class="form-group">Caducidad: <input type="date" id="caducidad" name="fecha" class="form-control" width="30em" value="" required="" placeholder="Cantidad">
                        </div>
                    </div>
                  </div> <!-- col -->  
                </div> <!-- row -->  
   </div>  <!-- Container -->   
 <div  class="container">  
  <div class="row" >
     <div class="col" align="center">
     <h3 >Selecciona impresora</h3>
            <h3>para este toner</h3>
      </div>
    </div>
  </div>

<div class="container p-4  my-3 border">
 <div class="form-group">       
  <div class="row">
   <div class="col-md-6 mx-auto" align="center">
      <label><select id="impresora" name="impresora" class="form-control "  >    
         <option value="SELECCIONA">Selecciona Impresora</option>
            <?php
                while($rowt = mysqli_fetch_array($imdis)){
                    echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
                }
              ?>                                                      
      </select></label>
      <br><br>
     </div><!-- col --> 
   </div><!-- row -->
<div class="row">
  <div class="col-md-6 mx-auto " align="center" >
    <div class="form-check">
   <input type="checkbox" class="form-check-input" id="cbox1" value="cbox1" onclick="myFunction()"><label class="form-check-label">¿Es compatible con otra impresora?</label><br>
   </div>
  </div>
</div>

   <div class="row">
    <div class="col-md-6 mx-auto " align="center">
      <label>
      <select id="impresora2" style="display:none" name="impresora2" class="form-control ">    
       <option value="SELECCIONA">Selecciona Impresora</option>
       <?php
          $imdis = mysqli_query($conn, $queryImDis);
           while($rowt = mysqli_fetch_array($imdis)){
             echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].'&nbsp;&nbsp; Modelo:&nbsp;'.$rowt[modelo].'</option>';
           }
       ?>
      </select></label>
    </div>
   </div>
   <!-- SEGUNDA IMPRESORA  -->
<div class="row" align="center">
  <div class="col-md-3.5 mx-auto " align="center">
    <div class="form-check"> 
    <label id="pregunta2" style="display:none"><input type="checkbox" class="form-check-input" id="cbox2" value="segundai" style="display:none"  onclick="myFunction()">¿Es compatible con otra impresora?
      </label>
    </div>
 </div> 
</div>
  
    <div class="row">
     <div class="col-md-6 mx-auto " align="center">
      <label>
        <select id="impresora3" style="display:none" name="impresora3" class="form-control" >    
           <option value="SELECCIONA">Selecciona Impresora</option>
        <?php
           $imdis = mysqli_query($conn, $queryImDis);
             while($rowt = mysqli_fetch_array($imdis)){
                echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
             }
             ?> 
       </select></label> 
      </div>  
     </div>
     <!-- TERCERA IMPRESORA  -->
  <div class="row">
    <div class="col-md-3.5 mx-auto " align="center">
       <div class="form-check"> 
        <input type="checkbox" id="cbox3" class="form-check-input" value="tercerai" style="display:none" onclick="myFunction()">
          <label id="pregunta3" style="display:none"> ¿Es compatible con otra impresora?</label><br>
      </div>
            <select id="impresora4" style="display:none" name="impresora4" class="form-control">    
            <option value="SELECCIONA">Selecciona Impresora</option>
             <?php
                $imdis = mysqli_query($conn, $queryImDis);
                  while($rowt = mysqli_fetch_array($imdis)){
                    echo '<option value="'.$rowt[id_impresora].'">'.$rowt[marca].' &nbsp;&nbsp; Modelo:'.$rowt[modelo].'</option>';
              }
               ?> 
           </select>                                                
    </div>
   </div>

         <div class="row" align="center">
          <div class="col">
          <button name="nToner" class="btn btn-success" type="submit" onclick="validaToner()">Guardar</button><a href="bandeja_toner.php" class="btn btn-outline-success" role="button">Regresar</a>
        </div>
      </div>
  
    </form>
   </div>  
  </div>

    <?php include("include/footer.php"); ?>

