<?php
   include("db.php");
   include("datos_edit.php");
   include("consultas.php");
  ?>
<?php include("include/header.php"); ?>



 <div class="card-body">  <h1 align="center" >Reporte de Impresora</h1>
 
<div class="container p-4">
    <div class="car">
      <div class="card-body"> 
        <form action="guardar_reporte.php?id=<?php echo $_GET['id']; ?>" method="POST" >
        <h3 align="center">Datos de la impresora</h3>     
     </div>
    </div>
<div class="row" align="center">
    <div class="car">
     <div class="card-body"> 
    <div class="form-inline">
    <div class="form-group">
     <div class="col-md-2" >       
       Marca&nbsp;&nbsp;&nbsp; <input name="id_impresora" value="<?php echo $id_impresora; ?>" type="hidden"  >
        <input id="marca" name="marca" value="<?php echo $marca;?>" type="text"  size="12" class="form-control" >
     </div>
     <div class="col-md-2">
       Modelo <input type="text" id="modelo" name="modelo" value="<?php echo $modelo; ?>" class="form-control" size="18" >
     </div>
      <div class="col-md-2">
       No. Serie: <input type="text" id="nserie" name="nserie" value="<?php echo $nSerie; ?>" class="form-control" size="18" >
     </div>
        <div class="col-md-2">Ubicación
         <select id="iubicacion" name="iubicacion" class="form-control ">
                                      <option value="<?php echo $id_ubicacion; ?>"><?php echo $direccion; ?>  <?php echo $ubicacion; ?></option>
                                  <?php while($rowu = mysqli_fetch_array($ubicacionq)){
                                       echo '<option value="'.$rowu[id_ubicacion].'">'.$rowu[direccion].' &nbsp;&nbsp; Ubicación:&nbsp;'.$rowu[ubicacion].'</option>';
                                      }
                                  ?>                                                      
                          </select>   
        </div>  
  </div>
   </div> 
   <br>
   <br>
   <br>
<div conteiner>   
  <div class="row">
    <div class="form-inline">
    <div class="form-group">
     <div class="col-md-3">
        <select id="tReporte" name="tReporte" class="form-control-sm ">
           <option value="FUNCIONANDO">SELECCIONA TIPO DE REPORTE</option>
                    <option value="CAMBIO DE TONER">CAMBIO DE TONER</option>
                    <option value="VERIFICACIÓN DE TONER">VERIFICACIÓN DE TONER</option>
                    <option value="CAMBIO DE UBICACIÓN">CAMBIO DE UBICACIÓN</option>
                    <option value="REPORTADA">REPORTADA</option>
                    <option value="EN REPARACIÓN">EN REPARACIÓN</option>
                    <option value="ENTREGADA">ENTREGADA</option>
                    <option value="FUNCIONANDO">FUNCIONANDO</option>
        </select>
     </div>  
     
        <div class="col-md-1.5">
     <input type="number" id="paginas" name="paginas" size="8" value="" required="" maxlength="5"  class="form-control-sm-8" placeholder="No. de impresiones">
     </div>
     
     <div class="col-md-2">
     <input type="number" id="porcentaje" name="porcentaje" value="" required="" min="1" max="100" maxlength="3"  class="form-control-sm" size="5" placeholder="% de tinta">
     </div>
     <div class="col-md-1.5">
      <input type="date"  name="fecha" class="form-control-sm" size="10" value="<?php echo date('Y-m-d') ?>">
    </div>
      <div class="col-md-2">
       <textarea id="descripcion" name="descripcion" rows="4" value="" required="" class="form-control" cols="50" placeholder="Descripción del problema"></textarea>
     </div>

     </div>
   </div>
 </div>
  </div> 
    </div>
                        <div align="left">
                       <button class="btn btn-success" name="reporte" type="submit"><i class="fas fa-save"></i>&nbsp;Guardar</button>

                <!--      <a href="#" class="btn btn-success" name="enviar" id="enviar"><i class="fas fa-envelope"></i>&nbsp;Reportar</a> -->

                       <a href="index.php" class="btn btn-outline-success" role="button">Regresar</a>
         </div>
               <div class="popup">
                <div class="popup-content" >
                  <label><img src="img/close.png" alt="close" width="30" height="60" class="close"></label><br>
                  <img src="img/mail.png" alt="User" width="40" height="40">
                  <label><input type="text" style="width : 390px; heigth : 5px" placeholder="Asunto" ></label>
                  <label><textarea id="descripcionEmail" name="descripcionEmail" class="form-control" cols="75" rows="8">Impresora marca: <?php echo $marca;?>   Modelo: <?php echo $modelo; ?>   N/S:<?php echo $nSerie; ?> </textarea></label>
                 <label><a href="enviar.php?id=descripcionEmail" class="button">Enviar</a></label>



                </div>
               </div>

               <script type="text/javascript">
  
document.getElementById("enviar").addEventListener("click",function(){
  document.querySelector(".popup").style.display = "flex";   
})                

document.querySelector(".close").addEventListener("click", function(){
   document.querySelector(".popup").style.display = "none";
})
</script>


   </form>
 </div>

<div class="conteiner-fluid" align="center">
         <table class="table table-bordered table-hover"> 
         <thead>
          <tr bgcolor="#cdcdcd">
            <th>No. Rep.</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>No. Serie</th>
            <th>Estado</th>
            <th>N. Hojas</th>
            <th align="center">Porcentaje %</th>
            <th>fecha</th>          
            <th width="12%">Reporte</th>
          </tr>
         </thead>
          <tbody>
            <?php
                while($row = mysqli_fetch_array($reportes)){?>
                  <tr>
                     <td width="8%"><?php echo $row['id_reportes'] ?></td>
                     <td width="15%"><?php echo $row['marca'] ?></td>
                     <td width="15%"><?php echo $row['modelo'] ?></td>
                     <td width="15%"><?php echo $row['nSerie'] ?></td>
                     <td width="12%"><?php echo $row['status'] ?></td>
                     <td width="5%"><?php echo $row['n_impresiones'] ?></td>
                     <td width="7%" align="center"><?php echo $row['porcentaje'] ?> %<progress class="progress-bar" value="<?php echo $row['porcentaje'] ?>" max=100></progress></td>
                     <td width="15%"><?php echo date('Y-m-d', strtotime($row['fecha']))?></td> 
                     <td width="7%">
                       <a href="reporte_listado.php?id=<?php echo $row['id_impresora']?>" class="btn btn-secondary"><i class="fas fa-clipboard-list" alt="editar" title="Reporte de Impresora"></i></a><a href="reporte_general.php?id=<?php echo $row['id_impresora']?>" class="btn btn-danger"><i class="fas fa-clipboard-list" alt="editar" title="Reporte General"></i></a></td>                 
                  </tr>
                 <?php } ?>
             </tbody>
        </table>
     
  </div>

<?php include("include/footer.php"); ?>
</body>
</html>
                        
    